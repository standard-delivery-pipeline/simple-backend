FROM openjdk:11

RUN useradd -ms /bin/bash spring

USER spring
WORKDIR /home/spring

COPY /build/libs/simple-backend-*.jar /app/application.jar

EXPOSE 8080

ENTRYPOINT ["java", "-XX:+UseContainerSupport", "-XX:MaxRAMPercentage=75.0", "-jar", "/app/application.jar"]