package com.vizgalov.syberry.simplebackend;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@SpringBootTest
@AutoConfigureMockMvc
class SimpleBackendApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void helloWorldTest() throws Exception {
        var actualResponse = mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/hello"))
            .andReturn().getResponse();

        assertEquals(200, actualResponse.getStatus());
        assertEquals("test", actualResponse.getContentAsString());
    }

}
