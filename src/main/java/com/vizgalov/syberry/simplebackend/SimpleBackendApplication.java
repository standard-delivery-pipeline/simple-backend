package com.vizgalov.syberry.simplebackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * Dummy backend project application.
 */
@SuppressWarnings("PMD.UseUtilityClass")
@SpringBootApplication
public class SimpleBackendApplication {

    /**
     * Dummy backend project application entry point.
     */
    public static void main(String[] args) {
        SpringApplication.run(SimpleBackendApplication.class, args);
    }

}
