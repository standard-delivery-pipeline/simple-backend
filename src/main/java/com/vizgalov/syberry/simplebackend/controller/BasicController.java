package com.vizgalov.syberry.simplebackend.controller;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Simple hello world controller.
 */
@RestController
@RequestMapping("api/v1")
public class BasicController {

    @Value("${app.message}")
    private String message;

    /**
     * Get method for retrieving hello world string.
     */
    @GetMapping("hello")
    public ResponseEntity<String> retrieveHelloWorld() {
        return ResponseEntity.ok(message);
    }

}
